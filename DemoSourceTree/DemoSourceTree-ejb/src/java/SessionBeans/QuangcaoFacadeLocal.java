/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Quangcao;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface QuangcaoFacadeLocal {

    void create(Quangcao quangcao);

    void edit(Quangcao quangcao);

    void remove(Quangcao quangcao);

    Quangcao find(Object id);

    List<Quangcao> findAll();

    List<Quangcao> findRange(int[] range);

    int count();
    
}
