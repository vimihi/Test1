/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Donhang;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface DonhangFacadeLocal {

    void create(Donhang donhang);

    void edit(Donhang donhang);

    void remove(Donhang donhang);

    Donhang find(Object id);

    List<Donhang> findAll();

    List<Donhang> findRange(int[] range);

    int count();
    
}
