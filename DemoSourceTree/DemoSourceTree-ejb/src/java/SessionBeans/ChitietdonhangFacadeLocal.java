/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Chitietdonhang;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface ChitietdonhangFacadeLocal {

    void create(Chitietdonhang chitietdonhang);

    void edit(Chitietdonhang chitietdonhang);

    void remove(Chitietdonhang chitietdonhang);

    Chitietdonhang find(Object id);

    List<Chitietdonhang> findAll();

    List<Chitietdonhang> findRange(int[] range);

    int count();
    
}
