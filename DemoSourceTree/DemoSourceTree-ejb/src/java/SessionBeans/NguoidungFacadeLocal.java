/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Nguoidung;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface NguoidungFacadeLocal {

    void create(Nguoidung nguoidung);

    void edit(Nguoidung nguoidung);

    void remove(Nguoidung nguoidung);

    Nguoidung find(Object id);

    List<Nguoidung> findAll();

    List<Nguoidung> findRange(int[] range);

    int count();
    
}
