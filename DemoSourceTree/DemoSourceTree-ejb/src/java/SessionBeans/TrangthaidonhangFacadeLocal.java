/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Trangthaidonhang;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface TrangthaidonhangFacadeLocal {

    void create(Trangthaidonhang trangthaidonhang);

    void edit(Trangthaidonhang trangthaidonhang);

    void remove(Trangthaidonhang trangthaidonhang);

    Trangthaidonhang find(Object id);

    List<Trangthaidonhang> findAll();

    List<Trangthaidonhang> findRange(int[] range);

    int count();
    
}
