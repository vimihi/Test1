/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Chitietdonhang;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Administrator
 */
@Stateless
public class ChitietdonhangFacade extends AbstractFacade<Chitietdonhang> implements ChitietdonhangFacadeLocal {

    @PersistenceContext(unitName = "DemoSourceTree-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ChitietdonhangFacade() {
        super(Chitietdonhang.class);
    }
    
}
