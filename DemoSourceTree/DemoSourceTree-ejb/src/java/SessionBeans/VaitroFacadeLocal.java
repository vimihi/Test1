/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Vaitro;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface VaitroFacadeLocal {

    void create(Vaitro vaitro);

    void edit(Vaitro vaitro);

    void remove(Vaitro vaitro);

    Vaitro find(Object id);

    List<Vaitro> findAll();

    List<Vaitro> findRange(int[] range);

    int count();
    
}
