/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Quangcao;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Administrator
 */
@Stateless
public class QuangcaoFacade extends AbstractFacade<Quangcao> implements QuangcaoFacadeLocal {

    @PersistenceContext(unitName = "DemoSourceTree-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QuangcaoFacade() {
        super(Quangcao.class);
    }
    
}
