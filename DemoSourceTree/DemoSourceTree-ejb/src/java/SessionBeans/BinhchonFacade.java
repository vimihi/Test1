/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Binhchon;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Administrator
 */
@Stateless
public class BinhchonFacade extends AbstractFacade<Binhchon> implements BinhchonFacadeLocal {

    @PersistenceContext(unitName = "DemoSourceTree-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BinhchonFacade() {
        super(Binhchon.class);
    }
    
}
