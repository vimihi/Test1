/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Sanpham;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface SanphamFacadeLocal {

    void create(Sanpham sanpham);

    void edit(Sanpham sanpham);

    void remove(Sanpham sanpham);

    Sanpham find(Object id);

    List<Sanpham> findAll();

    List<Sanpham> findRange(int[] range);

    int count();
    
}
