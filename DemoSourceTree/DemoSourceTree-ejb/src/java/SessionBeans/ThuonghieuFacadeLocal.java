/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Thuonghieu;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface ThuonghieuFacadeLocal {

    void create(Thuonghieu thuonghieu);

    void edit(Thuonghieu thuonghieu);

    void remove(Thuonghieu thuonghieu);

    Thuonghieu find(Object id);

    List<Thuonghieu> findAll();

    List<Thuonghieu> findRange(int[] range);

    int count();
    
}
