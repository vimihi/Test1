/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Binhchon;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface BinhchonFacadeLocal {

    void create(Binhchon binhchon);

    void edit(Binhchon binhchon);

    void remove(Binhchon binhchon);

    Binhchon find(Object id);

    List<Binhchon> findAll();

    List<Binhchon> findRange(int[] range);

    int count();
    
}
