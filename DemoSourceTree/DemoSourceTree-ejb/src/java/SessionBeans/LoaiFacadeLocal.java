/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Loai;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Administrator
 */
@Local
public interface LoaiFacadeLocal {

    void create(Loai loai);

    void edit(Loai loai);

    void remove(Loai loai);

    Loai find(Object id);

    List<Loai> findAll();

    List<Loai> findRange(int[] range);

    int count();
    
}
