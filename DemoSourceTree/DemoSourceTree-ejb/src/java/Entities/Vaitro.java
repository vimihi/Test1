/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "vaitro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vaitro.findAll", query = "SELECT v FROM Vaitro v"),
    @NamedQuery(name = "Vaitro.findById", query = "SELECT v FROM Vaitro v WHERE v.id = :id"),
    @NamedQuery(name = "Vaitro.findByTenvaitro", query = "SELECT v FROM Vaitro v WHERE v.tenvaitro = :tenvaitro")})
public class Vaitro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tenvaitro")
    private String tenvaitro;
    @Lob
    @Size(max = 65535)
    @Column(name = "mota")
    private String mota;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVaitro")
    private List<Nguoidung> nguoidungList;

    public Vaitro() {
    }

    public Vaitro(Integer id) {
        this.id = id;
    }

    public Vaitro(Integer id, String tenvaitro) {
        this.id = id;
        this.tenvaitro = tenvaitro;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenvaitro() {
        return tenvaitro;
    }

    public void setTenvaitro(String tenvaitro) {
        this.tenvaitro = tenvaitro;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    @XmlTransient
    public List<Nguoidung> getNguoidungList() {
        return nguoidungList;
    }

    public void setNguoidungList(List<Nguoidung> nguoidungList) {
        this.nguoidungList = nguoidungList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vaitro)) {
            return false;
        }
        Vaitro other = (Vaitro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Vaitro[ id=" + id + " ]";
    }
    
}
