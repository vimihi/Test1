/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "quangcao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Quangcao.findAll", query = "SELECT q FROM Quangcao q"),
    @NamedQuery(name = "Quangcao.findById", query = "SELECT q FROM Quangcao q WHERE q.id = :id"),
    @NamedQuery(name = "Quangcao.findByHinhanh", query = "SELECT q FROM Quangcao q WHERE q.hinhanh = :hinhanh"),
    @NamedQuery(name = "Quangcao.findByThongdiep", query = "SELECT q FROM Quangcao q WHERE q.thongdiep = :thongdiep"),
    @NamedQuery(name = "Quangcao.findByNgaydang", query = "SELECT q FROM Quangcao q WHERE q.ngaydang = :ngaydang")})
public class Quangcao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "hinhanh")
    private String hinhanh;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "thongdiep")
    private String thongdiep;
    @Lob
    @Size(max = 65535)
    @Column(name = "thongtinchitiet")
    private String thongtinchitiet;
    @Column(name = "ngaydang")
    @Temporal(TemporalType.DATE)
    private Date ngaydang;

    public Quangcao() {
    }

    public Quangcao(Integer id) {
        this.id = id;
    }

    public Quangcao(Integer id, String hinhanh, String thongdiep) {
        this.id = id;
        this.hinhanh = hinhanh;
        this.thongdiep = thongdiep;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public String getThongdiep() {
        return thongdiep;
    }

    public void setThongdiep(String thongdiep) {
        this.thongdiep = thongdiep;
    }

    public String getThongtinchitiet() {
        return thongtinchitiet;
    }

    public void setThongtinchitiet(String thongtinchitiet) {
        this.thongtinchitiet = thongtinchitiet;
    }

    public Date getNgaydang() {
        return ngaydang;
    }

    public void setNgaydang(Date ngaydang) {
        this.ngaydang = ngaydang;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Quangcao)) {
            return false;
        }
        Quangcao other = (Quangcao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Quangcao[ id=" + id + " ]";
    }
    
}
