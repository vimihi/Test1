/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "chitietdonhang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Chitietdonhang.findAll", query = "SELECT c FROM Chitietdonhang c"),
    @NamedQuery(name = "Chitietdonhang.findById", query = "SELECT c FROM Chitietdonhang c WHERE c.id = :id"),
    @NamedQuery(name = "Chitietdonhang.findBySoluong", query = "SELECT c FROM Chitietdonhang c WHERE c.soluong = :soluong")})
public class Chitietdonhang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "soluong")
    private int soluong;
    @JoinColumn(name = "id_donhang", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Donhang idDonhang;
    @JoinColumn(name = "id_sanpham", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sanpham idSanpham;

    public Chitietdonhang() {
    }

    public Chitietdonhang(Integer id) {
        this.id = id;
    }

    public Chitietdonhang(Integer id, int soluong) {
        this.id = id;
        this.soluong = soluong;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public Donhang getIdDonhang() {
        return idDonhang;
    }

    public void setIdDonhang(Donhang idDonhang) {
        this.idDonhang = idDonhang;
    }

    public Sanpham getIdSanpham() {
        return idSanpham;
    }

    public void setIdSanpham(Sanpham idSanpham) {
        this.idSanpham = idSanpham;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Chitietdonhang)) {
            return false;
        }
        Chitietdonhang other = (Chitietdonhang) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Chitietdonhang[ id=" + id + " ]";
    }
    
}
