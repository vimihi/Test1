/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "donhang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Donhang.findAll", query = "SELECT d FROM Donhang d"),
    @NamedQuery(name = "Donhang.findById", query = "SELECT d FROM Donhang d WHERE d.id = :id"),
    @NamedQuery(name = "Donhang.findByNgaydathang", query = "SELECT d FROM Donhang d WHERE d.ngaydathang = :ngaydathang"),
    @NamedQuery(name = "Donhang.findByTennguoinhanhang", query = "SELECT d FROM Donhang d WHERE d.tennguoinhanhang = :tennguoinhanhang"),
    @NamedQuery(name = "Donhang.findByDienthoainguoinhan", query = "SELECT d FROM Donhang d WHERE d.dienthoainguoinhan = :dienthoainguoinhan"),
    @NamedQuery(name = "Donhang.findByDiachigiaohang", query = "SELECT d FROM Donhang d WHERE d.diachigiaohang = :diachigiaohang"),
    @NamedQuery(name = "Donhang.findByThanhtoan", query = "SELECT d FROM Donhang d WHERE d.thanhtoan = :thanhtoan")})
public class Donhang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ngaydathang")
    @Temporal(TemporalType.DATE)
    private Date ngaydathang;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "tennguoinhanhang")
    private String tennguoinhanhang;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "dienthoainguoinhan")
    private String dienthoainguoinhan;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "diachigiaohang")
    private String diachigiaohang;
    @Lob
    @Size(max = 65535)
    @Column(name = "ghichu")
    private String ghichu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "thanhtoan")
    private short thanhtoan;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDonhang")
    private List<Chitietdonhang> chitietdonhangList;
    @JoinColumn(name = "id_khachhang", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Nguoidung idKhachhang;
    @JoinColumn(name = "id_trangthai", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Trangthaidonhang idTrangthai;

    public Donhang() {
    }

    public Donhang(Integer id) {
        this.id = id;
    }

    public Donhang(Integer id, Date ngaydathang, String tennguoinhanhang, String dienthoainguoinhan, String diachigiaohang, short thanhtoan) {
        this.id = id;
        this.ngaydathang = ngaydathang;
        this.tennguoinhanhang = tennguoinhanhang;
        this.dienthoainguoinhan = dienthoainguoinhan;
        this.diachigiaohang = diachigiaohang;
        this.thanhtoan = thanhtoan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getNgaydathang() {
        return ngaydathang;
    }

    public void setNgaydathang(Date ngaydathang) {
        this.ngaydathang = ngaydathang;
    }

    public String getTennguoinhanhang() {
        return tennguoinhanhang;
    }

    public void setTennguoinhanhang(String tennguoinhanhang) {
        this.tennguoinhanhang = tennguoinhanhang;
    }

    public String getDienthoainguoinhan() {
        return dienthoainguoinhan;
    }

    public void setDienthoainguoinhan(String dienthoainguoinhan) {
        this.dienthoainguoinhan = dienthoainguoinhan;
    }

    public String getDiachigiaohang() {
        return diachigiaohang;
    }

    public void setDiachigiaohang(String diachigiaohang) {
        this.diachigiaohang = diachigiaohang;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public short getThanhtoan() {
        return thanhtoan;
    }

    public void setThanhtoan(short thanhtoan) {
        this.thanhtoan = thanhtoan;
    }

    @XmlTransient
    public List<Chitietdonhang> getChitietdonhangList() {
        return chitietdonhangList;
    }

    public void setChitietdonhangList(List<Chitietdonhang> chitietdonhangList) {
        this.chitietdonhangList = chitietdonhangList;
    }

    public Nguoidung getIdKhachhang() {
        return idKhachhang;
    }

    public void setIdKhachhang(Nguoidung idKhachhang) {
        this.idKhachhang = idKhachhang;
    }

    public Trangthaidonhang getIdTrangthai() {
        return idTrangthai;
    }

    public void setIdTrangthai(Trangthaidonhang idTrangthai) {
        this.idTrangthai = idTrangthai;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Donhang)) {
            return false;
        }
        Donhang other = (Donhang) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Donhang[ id=" + id + " ]";
    }
    
}
