/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "trangthaidonhang")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Trangthaidonhang.findAll", query = "SELECT t FROM Trangthaidonhang t"),
    @NamedQuery(name = "Trangthaidonhang.findById", query = "SELECT t FROM Trangthaidonhang t WHERE t.id = :id"),
    @NamedQuery(name = "Trangthaidonhang.findByTentrangthai", query = "SELECT t FROM Trangthaidonhang t WHERE t.tentrangthai = :tentrangthai")})
public class Trangthaidonhang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "tentrangthai")
    private String tentrangthai;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTrangthai")
    private List<Donhang> donhangList;

    public Trangthaidonhang() {
    }

    public Trangthaidonhang(Integer id) {
        this.id = id;
    }

    public Trangthaidonhang(Integer id, String tentrangthai) {
        this.id = id;
        this.tentrangthai = tentrangthai;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTentrangthai() {
        return tentrangthai;
    }

    public void setTentrangthai(String tentrangthai) {
        this.tentrangthai = tentrangthai;
    }

    @XmlTransient
    public List<Donhang> getDonhangList() {
        return donhangList;
    }

    public void setDonhangList(List<Donhang> donhangList) {
        this.donhangList = donhangList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trangthaidonhang)) {
            return false;
        }
        Trangthaidonhang other = (Trangthaidonhang) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Trangthaidonhang[ id=" + id + " ]";
    }
    
}
