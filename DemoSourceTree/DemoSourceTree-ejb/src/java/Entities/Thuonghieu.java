/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "thuonghieu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Thuonghieu.findAll", query = "SELECT t FROM Thuonghieu t"),
    @NamedQuery(name = "Thuonghieu.findById", query = "SELECT t FROM Thuonghieu t WHERE t.id = :id"),
    @NamedQuery(name = "Thuonghieu.findByTenthuonghieu", query = "SELECT t FROM Thuonghieu t WHERE t.tenthuonghieu = :tenthuonghieu"),
    @NamedQuery(name = "Thuonghieu.findByHinhanh", query = "SELECT t FROM Thuonghieu t WHERE t.hinhanh = :hinhanh")})
public class Thuonghieu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "tenthuonghieu")
    private String tenthuonghieu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "hinhanh")
    private String hinhanh;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idThuonghieu")
    private List<Sanpham> sanphamList;

    public Thuonghieu() {
    }

    public Thuonghieu(Integer id) {
        this.id = id;
    }

    public Thuonghieu(Integer id, String tenthuonghieu, String hinhanh) {
        this.id = id;
        this.tenthuonghieu = tenthuonghieu;
        this.hinhanh = hinhanh;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenthuonghieu() {
        return tenthuonghieu;
    }

    public void setTenthuonghieu(String tenthuonghieu) {
        this.tenthuonghieu = tenthuonghieu;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    @XmlTransient
    public List<Sanpham> getSanphamList() {
        return sanphamList;
    }

    public void setSanphamList(List<Sanpham> sanphamList) {
        this.sanphamList = sanphamList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Thuonghieu)) {
            return false;
        }
        Thuonghieu other = (Thuonghieu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Thuonghieu[ id=" + id + " ]";
    }
    
}
