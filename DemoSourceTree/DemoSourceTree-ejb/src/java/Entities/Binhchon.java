/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "binhchon")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Binhchon.findAll", query = "SELECT b FROM Binhchon b"),
    @NamedQuery(name = "Binhchon.findById", query = "SELECT b FROM Binhchon b WHERE b.id = :id"),
    @NamedQuery(name = "Binhchon.findByTen", query = "SELECT b FROM Binhchon b WHERE b.ten = :ten"),
    @NamedQuery(name = "Binhchon.findByEmail", query = "SELECT b FROM Binhchon b WHERE b.email = :email"),
    @NamedQuery(name = "Binhchon.findByDiem", query = "SELECT b FROM Binhchon b WHERE b.diem = :diem")})
public class Binhchon implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ten")
    private String ten;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "diem")
    private int diem;
    @JoinColumn(name = "id_sanpham", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sanpham idSanpham;

    public Binhchon() {
    }

    public Binhchon(Integer id) {
        this.id = id;
    }

    public Binhchon(Integer id, String ten, String email, int diem) {
        this.id = id;
        this.ten = ten;
        this.email = email;
        this.diem = diem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDiem() {
        return diem;
    }

    public void setDiem(int diem) {
        this.diem = diem;
    }

    public Sanpham getIdSanpham() {
        return idSanpham;
    }

    public void setIdSanpham(Sanpham idSanpham) {
        this.idSanpham = idSanpham;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Binhchon)) {
            return false;
        }
        Binhchon other = (Binhchon) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Binhchon[ id=" + id + " ]";
    }
    
}
