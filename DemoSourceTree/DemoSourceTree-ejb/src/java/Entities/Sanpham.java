/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "sanpham")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sanpham.findAll", query = "SELECT s FROM Sanpham s"),
    @NamedQuery(name = "Sanpham.findById", query = "SELECT s FROM Sanpham s WHERE s.id = :id"),
    @NamedQuery(name = "Sanpham.findByTensanpham", query = "SELECT s FROM Sanpham s WHERE s.tensanpham = :tensanpham"),
    @NamedQuery(name = "Sanpham.findByHinhanh", query = "SELECT s FROM Sanpham s WHERE s.hinhanh = :hinhanh"),
    @NamedQuery(name = "Sanpham.findByDongia", query = "SELECT s FROM Sanpham s WHERE s.dongia = :dongia"),
    @NamedQuery(name = "Sanpham.findByDongiaKM", query = "SELECT s FROM Sanpham s WHERE s.dongiaKM = :dongiaKM"),
    @NamedQuery(name = "Sanpham.findBySoluong", query = "SELECT s FROM Sanpham s WHERE s.soluong = :soluong"),
    @NamedQuery(name = "Sanpham.findByNgaytao", query = "SELECT s FROM Sanpham s WHERE s.ngaytao = :ngaytao"),
    @NamedQuery(name = "Sanpham.findByHienthi", query = "SELECT s FROM Sanpham s WHERE s.hienthi = :hienthi")})
public class Sanpham implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "tensanpham")
    private String tensanpham;
    @Lob
    @Size(max = 65535)
    @Column(name = "mota")
    private String mota;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "hinhanh")
    private String hinhanh;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dongia")
    private double dongia;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "dongiaKM")
    private Double dongiaKM;
    @Basic(optional = false)
    @NotNull
    @Column(name = "soluong")
    private int soluong;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ngaytao")
    @Temporal(TemporalType.DATE)
    private Date ngaytao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hienthi")
    private int hienthi;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSanpham")
    private List<Chitietdonhang> chitietdonhangList;
    @JoinColumn(name = "id_loai", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Loai idLoai;
    @JoinColumn(name = "id_thuonghieu", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Thuonghieu idThuonghieu;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSanpham")
    private List<Binhchon> binhchonList;

    public Sanpham() {
    }

    public Sanpham(Integer id) {
        this.id = id;
    }

    public Sanpham(Integer id, String tensanpham, String hinhanh, double dongia, int soluong, Date ngaytao, int hienthi) {
        this.id = id;
        this.tensanpham = tensanpham;
        this.hinhanh = hinhanh;
        this.dongia = dongia;
        this.soluong = soluong;
        this.ngaytao = ngaytao;
        this.hienthi = hienthi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTensanpham() {
        return tensanpham;
    }

    public void setTensanpham(String tensanpham) {
        this.tensanpham = tensanpham;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public double getDongia() {
        return dongia;
    }

    public void setDongia(double dongia) {
        this.dongia = dongia;
    }

    public Double getDongiaKM() {
        return dongiaKM;
    }

    public void setDongiaKM(Double dongiaKM) {
        this.dongiaKM = dongiaKM;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public Date getNgaytao() {
        return ngaytao;
    }

    public void setNgaytao(Date ngaytao) {
        this.ngaytao = ngaytao;
    }

    public int getHienthi() {
        return hienthi;
    }

    public void setHienthi(int hienthi) {
        this.hienthi = hienthi;
    }

    @XmlTransient
    public List<Chitietdonhang> getChitietdonhangList() {
        return chitietdonhangList;
    }

    public void setChitietdonhangList(List<Chitietdonhang> chitietdonhangList) {
        this.chitietdonhangList = chitietdonhangList;
    }

    public Loai getIdLoai() {
        return idLoai;
    }

    public void setIdLoai(Loai idLoai) {
        this.idLoai = idLoai;
    }

    public Thuonghieu getIdThuonghieu() {
        return idThuonghieu;
    }

    public void setIdThuonghieu(Thuonghieu idThuonghieu) {
        this.idThuonghieu = idThuonghieu;
    }

    @XmlTransient
    public List<Binhchon> getBinhchonList() {
        return binhchonList;
    }

    public void setBinhchonList(List<Binhchon> binhchonList) {
        this.binhchonList = binhchonList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sanpham)) {
            return false;
        }
        Sanpham other = (Sanpham) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Sanpham[ id=" + id + " ]";
    }
    
}
